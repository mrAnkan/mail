package se.holm.daniel.mail.model

import android.content.ContentResolver
import android.net.Uri
import se.holm.daniel.mail.db.entity.Account

/**
 * Handles smtp instances for several accounts.
 */
object SmtpManager {
    // Holds smtp instances by account name.
    private val smtpInstances = HashMap<String, SmtpInstance>()

    /**
     * Use smtp instance for provided account name to send a message.
     */
    fun sendMessage(
        account: Account,
        recipients: Array<String>,
        subject: String,
        message: String,
        attachments: ArrayList<Uri>?,
        contentResolver: ContentResolver?
    ) {
        val current = getInstance(account)

        current?.sendMessage(recipients, subject, message, attachments, contentResolver)
    }

    /**
     * Create or use an existing smtp instance for provided account.
     */
    private fun getInstance(account: Account): SmtpInstance? {
        return if(smtpInstances.containsKey(account.accountName)) {
            // Use existing instance.
            smtpInstances[account.accountName]
        } else {
            // Create new instance.
            val created = SmtpInstance(account)
            smtpInstances[account.accountName] = created

            // Return created instance.
            created
        }
    }
}