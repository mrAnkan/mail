package se.holm.daniel.mail.model

import android.arch.lifecycle.MutableLiveData
import com.sun.mail.imap.IMAPFolder
import se.holm.daniel.mail.db.entity.Account
import java.text.SimpleDateFormat
import java.util.*
import javax.mail.*
import kotlin.collections.ArrayList

/**
 * Manages connection to imap for an account.
 */
class ImapInstance(private val account: Account) {

    // Holds settings for javamail to be used when communicating with imap.
    private val properties = System.getProperties()
    // Current JavaMail imap session and store for settings.
    private val session: Session = Session.getInstance(properties, null)
    private lateinit var store: Store

    /**
     * Get list of names on imap folders.
     */
    fun folders(): ArrayList<String> {
        val result = ArrayList<String>()

        val folders = store.defaultFolder.list()

        for(folder in folders) {
            result.add(folder.name)
        }

        return result
    }

    /**
     * Get messages for current selected folder. Fetches all messages every time. Expects the folder to contain just a
     * few messages.
     */
    fun getMessages(folderName: String, messagesLive: MutableLiveData<ArrayList<MessageHolder>>) {
        val messages = ArrayList<MessageHolder>()
        val format = SimpleDateFormat("d MMM yyyy", Locale.getDefault())
        val folder = store.getFolder(folderName) as IMAPFolder
        folder.open(Folder.READ_WRITE)

        for(message in folder.messages) {
            var html = false
            var content = ""

            if(message.content is String) {
                html = message.isMimeType("text/html")
                content = message.content as String
            } else if (message.content is Multipart) {
                val current = extractFromMultiPart(message.content as Multipart)
                if(current != null) {
                    html = current.second
                    content  = current.first
                }
            }
            messages.add(MessageHolder(
                message.subject,
                message.from.joinToString(", "),
                format.format(message.sentDate),
                content,
                html
            ))
            // Post each message to list, which then updates the ui.
            // Makes ui feel more responsive than just sending the
            // entire list when all messages has finished processing.
            messagesLive.postValue(messages)
        }

        folder.close(false)
    }

    /**
     * Extracts html or plaintext from a message part.
     */
    private fun getContent(message: Part): Pair<String, Boolean>? {
        if(message.content is String) {
            return Pair(message.content as String, message.isMimeType("text/html"))
        }

        return null
    }

    /**
     * Extract message text from multipart. Recurse and loop through a multipart message until text or html is found.
     * Prefers html over plain text.
     */
    private fun extractFromMultiPart(message: Multipart): Pair<String, Boolean>? {
        var i = 0
        var result: Pair<String, Boolean>? = null

        while (i < message.count) {
            val current = getContent(message.getBodyPart(i))
            if(current != null && current.second) {
                return current
            } else {
                result = current
            }
            i++
        }

        return result
    }

    /**
     * Setup a connection to imap-server and load default folder.
     */
    fun connect() {
        // No need to reconnect if is already connected.
        if(::store.isInitialized && store.isConnected)
            return

        // Use tls if possible.
        properties["mail.imap.starttls.enable"] = true

        // Setup connection details.
        val url = URLName(
            "imaps",
            account.imapHost,
            account.imapPort,
            null,
            account.imapUser,
            account.imapPassword
        )

        store = session.getStore(url)
        store.connect()
    }

    /**
     * Closes connection to imap-server.
     */
    fun disconnect()  {
        // Only try to disconnect if connected.
        if(::store.isInitialized && store.isConnected)
            store.close()
    }
}