package se.holm.daniel.mail.ui

import android.app.Activity
import android.app.AlertDialog
import android.content.ClipData
import android.content.DialogInterface
import android.content.Intent
import android.content.Intent.ACTION_OPEN_DOCUMENT
import android.content.Intent.EXTRA_ALLOW_MULTIPLE
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.Toast
import se.holm.daniel.mail.R

private const val FILE_REQUEST_CODE = 0

/**
 * This activity is used for composing a message. Sends user input back to previous activity.
 */
class ComposeMessageActivity: AppCompatActivity() {
    // Let users add multiple attachments.
    private var attachments: ArrayList<Uri> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compose_message)
    }

    /**
     * Let user select multiple files as attachments to the current message. Uses the storage access framework.
     */
    fun addAttachment(view: View) {
        Intent(ACTION_OPEN_DOCUMENT).apply {
            // Allow any openable file type.
            type = "*/*"
            addCategory(Intent.CATEGORY_OPENABLE)
            // Allow multiple attachments.
            putExtra(EXTRA_ALLOW_MULTIPLE, true)
        }.also {openDocumentIntent ->
            openDocumentIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(openDocumentIntent, FILE_REQUEST_CODE)
            }
        }
    }

    /**
     * This action is triggered by user for extracting provided details about current message and sending it as a
     * callback to previous activity.
     */
    fun sendMail(view: View) {
        // Get text from all of the input fields.
        val email = findViewById<EditText>(R.id.editRecipient).text.toString()
        val recipients =  email.split(",").map { it.trim() }.toTypedArray()
        val subject = findViewById<EditText>(R.id.editSubject).text.toString()
        val message = findViewById<EditText>(R.id.editMessage).text.toString()

        val intent = Intent()

        intent.apply {
            // Put all message details into the intent.
            putExtra(Intent.EXTRA_EMAIL, recipients)
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, message)

            // Only try to add attachments when there is at least one selected attachment.
            if(attachments.size > 0) {
                putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachments)
            }
        }

        // Callback to previous activity.
        setResult(Activity.RESULT_OK, intent)

        // End this activity.
        finish()
    }

    /**
     * Expects and handles result from intent for file selection.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        // Handle result of multiple user selected files from the storage access framework.
        if(requestCode == FILE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // Multiple selections is allowed, the result is treated as clip data.
            val multipleAttachments: ClipData? = intent?.clipData
            val singleAttachment = intent?.data

            // Only try to store uri in list of attachments when there is data.
            if(multipleAttachments != null) {
                var i = 0
                while(i < multipleAttachments.itemCount) {
                    val attachmentUri = multipleAttachments.getItemAt(i).uri
                    attachments.add(attachmentUri)
                    i++
                }
            } else if(singleAttachment != null) {
                attachments.add(singleAttachment)
            }
        }
    }

    /**
     * Add custom functionality for when back arrow in toolbar is pressed.
     */
    override fun onSupportNavigateUp(): Boolean {
        showWarning()
        return false
    }

    /**
     * Add custom functionality for when android back button is pressed.
     */
    override fun onBackPressed() {
        showWarning()
    }

    /**
     * Warn user that their message will be discarded when leaving the activity.
     */
    private fun showWarning() {
        val context = this
        AlertDialog.Builder(this).apply {
            setTitle(R.string.discard_message)
            setMessage(R.string.perm_deleted)
            setPositiveButton(R.string.ok) { _: DialogInterface, _: Int ->
                finish()
            }
            setNegativeButton(R.string.cancel) { _: DialogInterface, _: Int ->
                Toast.makeText(context, R.string.returned_to_message, Toast.LENGTH_SHORT).show()
            }
            show()
        }
    }
}