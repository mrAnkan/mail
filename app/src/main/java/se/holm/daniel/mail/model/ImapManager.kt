package se.holm.daniel.mail.model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import se.holm.daniel.mail.db.entity.Account

/**
 * Manages imap instances for several accounts. Holds a map over accounts and associated instance.
 */
object ImapManager {
    // Holds imap instances by account name.
    private val imapInstances = HashMap<String, ImapInstance>()
    private val currentFolders = MutableLiveData<ArrayList<String>>()

    private var currentMessages = MutableLiveData<ArrayList<MessageHolder>>()

    /**
     * Get non-mutable live data holding list of folders.
     */
    fun getFolders(): LiveData<ArrayList<String>> {
        return currentFolders
    }

    /**
     * Get non-mutable live data holding list of messages.
     */
    fun getMessages(): LiveData<ArrayList<MessageHolder>> {
        return currentMessages
    }

    /**
     * Makes sure account is connected.
     */
    fun connectAccount(account: Account?) {
        if(account != null) {
            getImapInstance(account)?.connect()
        }
    }

    /**
     * Makes sure account is disconnected.
     */
    fun disconnectAccount(account: Account?) {
        if(account != null) {
            getImapInstance(account)?.disconnect()
        }
    }

    /**
     * Retrieve messages for current account and folder.
     */
    fun loadMessages(account: Account?, folder: String) {
        if(account != null) {
            getImapInstance(account)?.getMessages(folder, currentMessages)
        }
    }

    /**
     * Loads folders for the provided account.
     */
    fun loadFolders(account: Account?) {
        if(account != null) {
            val instance = getImapInstance(account)
            currentFolders.postValue(instance?.folders())
        }
    }

    /**
     * Returns object that manages imap connections for a
     * specific account.
     */
    private fun getImapInstance(account: Account): ImapInstance? {
        val accountName = account.accountName
        return if(hasImapInstance(accountName)) {
            imapInstances[accountName]
        } else {
            createImapInstance(account)
        }
    }

    /**
     * Create instance of class that manages imap connections
     * for a specific account.
     */
    private fun createImapInstance(account: Account): ImapInstance? {
        val instance = ImapInstance(account)
        imapInstances[account.accountName] = instance

        // Returns instance.
        return instance
    }

    /**
     * Check if an account has an associated imap instance.
     */
    private fun hasImapInstance(accountName: String): Boolean {
        return imapInstances[accountName] != null
    }
}