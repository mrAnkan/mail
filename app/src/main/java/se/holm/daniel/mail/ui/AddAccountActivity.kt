package se.holm.daniel.mail.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import se.holm.daniel.mail.R

/**
 * This class handles the activity that a user accesses when adding an account to storage.
 */
class AddAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_account)
    }

    /**
     * Make it possible for user to press button in ui to save an account. Extracts data from all input fileds and sends
     * back to previous activity.
     */
    fun initiateSaveAccount(view: View) {
        // Extract generic account details.
        val accountName = findViewById<EditText>(R.id.editAccountName).text.toString()
        val fullName = findViewById<EditText>(R.id.editFullName).text.toString()

        // Extract imap details.
        val imapHost = findViewById<EditText>(R.id.editImapHost).text.toString()
        val imapPortString = findViewById<EditText>(R.id.editImapPort).text.toString()
        // Make sure imap port has a default value, otherwise app crashes.
        var imapPort = 993
        if(imapPortString != "") {
            imapPort = imapPortString.toInt()
        }

        val imapUser = findViewById<EditText>(R.id.editImapUser).text.toString()
        val imapPassword = findViewById<EditText>(R.id.editImapPassword).text.toString()

        // Extract smtp details
        val smtpHost = findViewById<EditText>(R.id.editSmtpHost).text.toString()
        val smtpPortString = findViewById<EditText>(R.id.editSmtpPort).text.toString()
        // Make sure smtp port has a default value, otherwise app crashes.
        var smtpPort = 587
        if(smtpPortString != "") {
            smtpPort = smtpPortString.toInt()
        }
        val emailAddress = findViewById<EditText>(R.id.editEmailAddress).text.toString()
        val smtpUser = findViewById<EditText>(R.id.editSmtpUser).text.toString()
        val smtpPassword = findViewById<EditText>(R.id.editSmtpPassword).text.toString()

        // Send values back to previous activity.
        val intent = Intent()
        intent.putExtra("accountName", accountName)
        intent.putExtra("fullName", fullName)
        intent.putExtra("imapHost", imapHost)
        intent.putExtra("imapPort", imapPort)
        intent.putExtra("imapUser", imapUser)
        intent.putExtra("imapPassword", imapPassword)
        intent.putExtra("smtpHost", smtpHost)
        intent.putExtra("smtpPort", smtpPort)
        intent.putExtra("emailAddress", emailAddress)
        intent.putExtra("smtpUser", smtpUser)
        intent.putExtra("smtpPassword", smtpPassword)

        setResult(Activity.RESULT_OK, intent)

        // End this activity.
        finish()
    }
}