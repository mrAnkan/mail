package se.holm.daniel.mail.ui

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import se.holm.daniel.mail.model.MessageHolder
import se.holm.daniel.mail.R
import kotlin.collections.ArrayList

/**
 * This class handles each row in the list of messages. Reuses objects for each row when scrolled back into view.
 */
class MessageListViewAdapter(context: Context): RecyclerView.Adapter<MessageListViewAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var itemClickListener: ItemClickListener? = null
    private var messages = emptyList<MessageHolder>()

    /**
     * Instantiates row xml file into corresponding view objects.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.row_recyclerview, parent, false)
        return ViewHolder(view)
    }

    /**
     * Bind data to each row.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = messages[position]

        holder.sender.text = entry.sender
        holder.subject.text =  entry.subject
        holder.date.text = entry.sentDate
    }

    /**
     * Set messages to be displayed in list.
     */
    fun setMessages(messages: ArrayList<MessageHolder>) {
        this.messages = messages
        notifyDataSetChanged()
    }

    /**
     * Total number of items available in list.
     */
    override fun getItemCount(): Int {
        return messages.size
    }

    /**
     * Return MessageHolder object for specific position in list.
     */
    fun getItem(position: Int): MessageHolder {
        return messages[position]
    }

    /**
     * Store and recycle views when they have been scrolled out of view.
     */
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var sender: TextView = itemView.findViewById(R.id.senderLabel)
        var subject: TextView = itemView.findViewById(R.id.subjectLabel)
        var date: TextView = itemView.findViewById(R.id.dateLabel)

        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            if (itemClickListener != null) itemClickListener!!.onItemClick(view, adapterPosition)
        }
    }

    /**
     * Used to set click listener on each row.
     */
    fun setClickListener(clickListener: ItemClickListener) {
        itemClickListener = clickListener
    }

    /**
     * Parent should implement this interface to make sure an action can be performed on click.
     */
    interface ItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}