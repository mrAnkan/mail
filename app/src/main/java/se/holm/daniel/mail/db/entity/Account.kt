package se.holm.daniel.mail.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
 * This class is a data holder object used both to describe the database storage of accounts and for getting data holder
 * objects when communicating with room.
 */
@Entity
data class Account(
    // Generic account information.
    @PrimaryKey(autoGenerate = true) val uid: Int,
    @ColumnInfo(name = "account_name") val accountName: String,
    @ColumnInfo(name = "full_name") val fullName: String?,

    // IMAP details.
    @ColumnInfo(name = "imap_host") val imapHost: String,
    @ColumnInfo(name = "imap_port") val imapPort: Int,
    @ColumnInfo(name = "imap_user") val imapUser: String,
    @ColumnInfo(name = "imap_password") val imapPassword: String,

    // SMTP details.
    @ColumnInfo(name = "smtp_host") val smtpHost: String,
    @ColumnInfo(name = "smtp_port") val smtpPort: Int,
    @ColumnInfo(name = "smtp_address") val mailAddress: String,
    @ColumnInfo(name = "smtp_user") val smtpUser: String,
    @ColumnInfo(name = "smtp_password") val smtpPassword: String
)