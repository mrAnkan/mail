package se.holm.daniel.mail

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.ContentResolver
import android.net.Uri
import se.holm.daniel.mail.db.MailDatabase
import se.holm.daniel.mail.db.entity.Account
import se.holm.daniel.mail.model.ImapManager
import se.holm.daniel.mail.model.MessageHolder
import se.holm.daniel.mail.model.SmtpManager

/**
 * Handles communication between viewmodels and models/ data managers. Acts as the one true source of data for this
 * application.
 */
object MailRepository {

    // Hold reference to storage of this application.
    private var database: MailDatabase? = null

    // Store and handle imap and smtp instances for several accounts.
    private val imapManager = ImapManager
    private val smtpManager = SmtpManager

    // Lock used to make sure that only one thread at a time can change current active account. This is to ensure that
    // the current account gets disconnected properly and the new account gets connected properly.
    private val accountLock = Object()
    private var currentAccount: Account? = null

    // This lock is used to make sure only one thread at a time can change the current active folder.
    private val messageLock = Object()
    private var currentFolder: String? = null

    // This lock is to ensure that only one thread can change what alert is to be displayed.
    private val alertLock = Object()
    private val alert = MutableLiveData<Pair<String?, String?>?>()

    /**
     * Makes sure that this repository has access to the database of this application.
     */
    fun initialize(database: MailDatabase) {
        // Enforces database reference to stay unchanged
        // after initialization.
        if(this.database == null)
            this.database = database
    }

    /**
     * Only activities can displayAlert dialogs, but errors are thrown in viewmodels, this is the communication link between
     * view and viewmodels when an alert is to be created.
     */
    fun alertFactory(): LiveData<Pair<String?, String?>?>  {
        return alert
    }

    /**
     * Let one viewmodel at a time set an alert to be created.
     */
    fun setAlert(current: Pair<String?, String?>?) {
        synchronized(alertLock) {
            alert.postValue(current)
        }
    }

    /**
     * Returns the account name of current account.
     */
    fun getCurrentAccountName(): String? {
        return currentAccount?.accountName
    }

    /**
     * Changes the current active account.
     */
    fun changeCurrentAccount(accountName: String) {
        // Make sure only one thread at a time can change the
        // currently active account.
        synchronized(accountLock) {
            if(!isCurrentAccount(accountName)) {
                // Disconnect current so there is only one imap
                // connection at a time.
                imapManager.disconnectAccount(currentAccount)

                // Change account and connect.
                currentAccount = getAccountByName(accountName)
                imapManager.connectAccount(currentAccount)
            }
        }
    }

    /**
     * Retrieves all accounts from the database.
     */
    fun getAccounts(): LiveData<List<Account>> {
        return database!!.accountDao().loadAll()
    }

    /**
     * Retrieves all folders for current active imap account.
     */
    fun getFolders(): LiveData<ArrayList<String>> {
        return imapManager.getFolders()
    }

    /**
     * Returns messages for current active folder.
     */
    fun getCurrentMessages(): LiveData<ArrayList<MessageHolder>> {
        return imapManager.getMessages()
    }

    /**
     * Loads messages list with messages for current active folder.
     */
    fun loadMessagesForFolder(folder: String) {
        if(!currentFolder.equals(folder))
            synchronized(messageLock) {
                imapManager.loadMessages(currentAccount, folder)
            }
    }

    /**
     * Loads folders for current account.
     */
    fun loadFolders() {
        // Make sure load does not happen at account change.
        synchronized(accountLock) {
            imapManager.loadFolders(currentAccount)
        }
    }

    /**
     * Inserts an account into database.
     */
    fun insertAccount(account: Account) {
        database!!.accountDao().insert(account)
    }

    /**
     * Invoke call to send email.
     */
    fun sendMessage(
        recipients: Array<String>,
        subject: String,
        message: String,
        attachments: ArrayList<Uri>?,
        contentResolver: ContentResolver?
    ) {
        smtpManager.sendMessage(currentAccount!!, recipients, subject, message, attachments, contentResolver)
    }

    /**
     * Find and return an account object using name of account.
     */
    private fun getAccountByName(accountName: String): Account? {
        return database!!.accountDao().findByName(accountName)
    }

    /**
     * Compare a provided name with the name of the current account.
     */
    private fun isCurrentAccount(accountName: String): Boolean {
        return accountName == currentAccount?.accountName
    }
}