package se.holm.daniel.mail.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.net.Uri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import se.holm.daniel.mail.MailApplication
import se.holm.daniel.mail.MailRepository
import se.holm.daniel.mail.model.MessageHolder
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

/**
 * Handles communication between views related to listing messages and data access repository.
 */
class MessageListViewModel(application: Application) : AndroidViewModel(application) {

    // Setup of kotlin concurrency helpers.
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    // Access application resources.
    private val app: MailApplication = application as MailApplication
    private val repository: MailRepository = app.getRepository()

    /**
     * Returns messages for current selected folder.
     */
    fun getMessages(): LiveData<ArrayList<MessageHolder>> {
        return repository.getCurrentMessages()
    }

    /**
     * Loads messages for a folder which belong to the current account. Is executed on another thread since network
     * actions is not allowed on main thread for performance reasons.
     */
    fun loadMessages(folder: String) = scope.launch(Dispatchers.IO) {
        try {
            repository.loadMessagesForFolder(folder)
        } catch (exception: Exception) {
            repository.setAlert(Pair("Could Not Load Folders!", "Could not load folders for the selected account!"))
        }
    }

    /**
     * Invoke call to send email using credentials of current selected account. The call is invoked on another thread
     * in order to not slow down the ui.
     */
    fun sendMessage(
        recipients: Array<String>,
        subject: String,
        message: String,
        attachments: ArrayList<Uri>?
    ) = scope.launch(Dispatchers.IO) {
        try {
            repository.sendMessage(recipients, subject, message, attachments, app.applicationContext.contentResolver)
        } catch (exception: Exception) {
            repository.setAlert(Pair("Could Not Send Message!", "The message could not be sent!"))
        }
    }
}