package se.holm.daniel.mail.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import se.holm.daniel.mail.db.entity.Account

/**
 * This interface or DAO, Data Access Object, represents functionality which accesses and extracts information about
 * stored accounts.
 */
@Dao
interface AccountDao {
    /**
     * Make it possible to retrieve all accounts and all updates made to the accounts.
     */
    @Query("SELECT * FROM account")
    fun loadAll(): LiveData<List<Account>>

    /**
     * Make it possible to find details about a specific account.
     */
    @Query("SELECT * FROM account WHERE account_name LIKE :accountName LIMIT 1")
    fun findByName(accountName: String): Account

    /**
     * Make it possible to insert an account into the database.
     */
    @Insert
    fun insert(account: Account)
}
