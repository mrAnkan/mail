package se.holm.daniel.mail

import android.app.AlertDialog
import android.app.Application
import android.arch.lifecycle.LiveData
import android.content.Context
import android.content.DialogInterface
import se.holm.daniel.mail.db.MailDatabase

/**
 * This class holds singeltons during application lifetime to prevent garbage collection.
 */
class MailApplication: Application() {

    private val mailRepository: MailRepository = MailRepository

    override fun onCreate() {
        super.onCreate()
        // Make sure repository has access to database.
        mailRepository.initialize(getDatabase())
    }

    /**
     * Display alert message to user when error occurred.
     */
    fun displayAlert(context: Context?, title: String?, message: String?) {
        if(context != null) {
            // Use default error title if passed title is null.
            val currentTitle: String = title ?: context.getString(R.string.error_occurred)

            AlertDialog.Builder(context).apply {
                setTitle(currentTitle)
                setMessage(message)
                setCancelable(false)
                setPositiveButton(R.string.ok) { _: DialogInterface, _: Int -> getRepository().setAlert(null) }

                show()
            }
        }
    }

    /**
     * Returns livedata to observe in views for creating alerts from viewmodels.
     */
    fun getAlertFactory(): LiveData<Pair<String?, String?>?> {
        return getRepository().alertFactory()
    }

    /**
     * Get the application database.
     */
    fun getDatabase(): MailDatabase {
        return MailDatabase.getDatabase(this)
    }

    /**
     * Get the repository which handles communication between ViewModels, storage and other operations, such as imap
     * and smtp.
     */
    fun getRepository(): MailRepository {
        return mailRepository
    }
}