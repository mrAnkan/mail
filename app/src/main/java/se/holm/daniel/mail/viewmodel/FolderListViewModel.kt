package se.holm.daniel.mail.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import se.holm.daniel.mail.MailApplication
import se.holm.daniel.mail.MailRepository

/**
 * Handles communication between views related to listing accounts and data access repository..
 */
class FolderListViewModel(application: Application): AndroidViewModel(application) {

    private val app: MailApplication = application as MailApplication
    private val repository: MailRepository = app.getRepository()

    /**
     * Returns LiveData containing folders for the current selected account.
     */
    fun getFolders(): LiveData<ArrayList<String>> {
        return repository.getFolders()
    }
}