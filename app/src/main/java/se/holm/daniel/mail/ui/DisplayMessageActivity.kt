package se.holm.daniel.mail.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_display_message.*
import se.holm.daniel.mail.R

/**
 * This activity displays details about a selected message.
 */
class DisplayMessageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_message)

        displayMessage()
    }

    /**
     * Display message and message details from intent passed by previous activity. Sets TextView and WebView contents.
     */
    private fun displayMessage() {
        // Extract message details from intent.
        val subject = intent.getStringExtra("subject")
        val sender = intent.getStringExtra("sender")
        val date = intent.getStringExtra("date")
        val isHtml = intent.getBooleanExtra("isHtml", false)
        var content = intent.getStringExtra("content")

        // Set message details in view using id of the views from xml.
        subjectHolder.text = subject
        senderHolder.text = sender
        dateHolder.text = date

        if(!isHtml) {
            // This html and css make sure that pre-formatted text is displayed within the view bounds.
            val head = "<html><head><meta name=\"viewport\" content=\"width=device-width, user-scalable=yes\" /></head>"
            val  open = "<body><pre style=\"font-family: sans-serif; word-wrap: break-word; overflow-wrap: break-word; hyphens: auto; white-space: pre-wrap;\">"
            val  close = "</pre></body></html>"

            // Construct a custom html string for plaintext messages.
            val html = head + open + content + close

            content = html
        }

        // Display messages using a webview.
        messageDisplay.loadData(content, "text/html", "UTF-8")
    }
}