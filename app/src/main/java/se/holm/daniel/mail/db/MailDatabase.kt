package se.holm.daniel.mail.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import se.holm.daniel.mail.db.dao.AccountDao
import se.holm.daniel.mail.db.entity.Account

/**
 *  Database singelton used to access the room storage of this application. Implements access to functionality of the
 *  interface of the only dao class used to access stored account details.
 */
@Database(entities = [Account::class], version = 1)
abstract class MailDatabase : RoomDatabase() {
    // Abstract functions that can be called from singelton object.
    abstract fun accountDao(): AccountDao

    // Singelton.
    companion object {
        // Make sure that any possible extra instances reads
        // same value.
        @Volatile
        private var INSTANCE: MailDatabase? = null

        /**
         * Retrieves and or creates database instance,
         * only one database instance is needed.
         */
        fun getDatabase(
            context: Context
        ): MailDatabase {
            // Only try to create an instance with a lock to prevent
            // any "liveness issues/ concurrency problems."
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    MailDatabase::class.java,
                    "mail_database"
                ).build()

                INSTANCE = instance

                instance
            }
        }
    }
}
