package se.holm.daniel.mail.ui

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import android.support.v4.widget.DrawerLayout
import android.support.design.widget.NavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import se.holm.daniel.mail.MailApplication
import se.holm.daniel.mail.R
import se.holm.daniel.mail.db.entity.Account
import se.holm.daniel.mail.viewmodel.AccountListViewModel
import se.holm.daniel.mail.viewmodel.FolderListViewModel
import se.holm.daniel.mail.viewmodel.MessageListViewModel

//  Request codes used to distinguish results from callbacks of activities.
private const val ADD_ACCOUNT_REQUEST = 0
private const val SEND_MESSAGE_REQUEST = 1

/**
 * This is the central activity, all other activities are children. This activity gets data from user interaction by
 * utilising view models. This activity forwards results from child activities to relevant viewmodels for processing.
 */
class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MessageListViewAdapter.ItemClickListener {

    // Variable for holding if should displayAlert folders or accounts in side menu.
    private var listFolders: Boolean = false
    // Hold a handy reference to side menu so can access current selected folder etc.
    private lateinit var sideMenu: Menu
    // Adapter injecting messages into list.
    private lateinit var adapter: MessageListViewAdapter

    // Setup of ViewModels, used to handle communication between content (or business logic) and this activity.
    private lateinit var accountListViewModel: AccountListViewModel
    private lateinit var folderListViewModel: FolderListViewModel
    private lateinit var messageListViewModel: MessageListViewModel

    // Setup of reference to application context so activity can access singeltons.
    private lateinit var app: MailApplication

    /**
     * Performs necessary setup of data in this activity. This method handles all fields or views that needs to be
     * initially configured for holding the correct dynamic data on first launch or any reload of activity.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        // Store handy reference to application context.
        app = applicationContext as MailApplication

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup of the top-bar.
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        // Creates and sets adapter for message list.
        initializeMessageListAdapter()

        // Setup of ViewModels, also makes sure to store references.
        initializeViewModels()

        // Used to invoke activity for message composition.
        val actionButton: FloatingActionButton = findViewById(R.id.fab)
        actionButton.setOnClickListener {
            val intent = Intent(this, ComposeMessageActivity::class.java)
            // Expecting result to forward to message viewmodel.
            if(app.getRepository().getCurrentAccountName() != null) {
                startActivityForResult(intent, SEND_MESSAGE_REQUEST)
            } else {
                // Only possible to compose message when an account is selected.
                app.displayAlert(
                    this,
                    "No Account Selected!",
                    "Select an account before composing a message!"
                )
            }
        }

        // Drawer layout for side menu.
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        // Make sure actions can be performed when pressing on folder in side menu.
        navigationView.setNavigationItemSelectedListener(this)

        // Store the reference of side menu for easy access.
        sideMenu = navigationView.menu

        // Add observers for updating ui on data changes.
        initializeObservers()

        // Make sure groups are toggled correctly.
        initialToggle()
    }

    /**
     * Create ViewModels to be used for communicating between
     * views and the data providers.
     */
    private fun initializeViewModels() {
        accountListViewModel = ViewModelProviders.of(this).get(AccountListViewModel::class.java)
        folderListViewModel = ViewModelProviders.of(this).get(FolderListViewModel::class.java)
        messageListViewModel = ViewModelProviders.of(this).get(MessageListViewModel::class.java)
    }

    /**
     * Initializes the observers used to update the ui when new data is added.
     */
    private fun initializeObservers() {
        // Listen for changes in account storage.
        accountListViewModel.accounts.observe(this, Observer { accounts->
            accounts?.let {
                // Set accounts in ui.
                initializeAccountGroup(it)
            }
        })

        // Listen for changes in folder storage.
        folderListViewModel.getFolders().observe(this, Observer { folders ->
            folders?.let {
                // Set folders in ui.
                initializeFolderGroup(it)
            }
        })

        // Listen for changes in imap message list.
        messageListViewModel.getMessages().observe(this, Observer { messages ->
            messages?.let {
                // Set messages in ui.
                adapter.setMessages(it)
            }
        })

        // Handle alerts created by viewmodels when an error has occurred.
        app.getAlertFactory().observe(this, Observer { details ->
            System.out.println("CHANGED")

            if(details != null)
                app.displayAlert(this, details?.first, details?.second)
        })
    }

    /**
     * Setup of adapter which handles list of messages.
     */
    private fun initializeMessageListAdapter() {
        val recyclerView: RecyclerView = findViewById(R.id.messageList)
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Add divider between each row.
        recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        // Create a new links adapter, used for handling entries.
        adapter = MessageListViewAdapter(this)

        // Add a function for callback when user press on entry.
        adapter.setClickListener(this)

        recyclerView.adapter = adapter
    }

    /**
     * Add custom functionality for when android back button is pressed.
     */
    override fun onBackPressed() {
        // Make sure drawer holding side menu is closed when pressing on back.
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    /**
     * Handle clicks on items in side menu.
     */
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.groupId) {
            R.id.menu_folders_group -> {
                // Load messages for selected folder.
                val folder = item.title.toString()
                messageListViewModel.loadMessages(folder)

                // Closes the side menu.
                val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            R.id.menu_accounts_group -> {
                val accountName = item.title.toString()

                // Change the current active account.
                accountListViewModel.changeCurrentAccount(accountName)

                // Tells menu to displayAlert folders.
                switchMenuState()

                // Toggle groups when account has been chosen.
                performToggleGroupsInSideMenu(accountName)
            }
            R.id.menu_accounts_settings_group -> {
                if(item.itemId == R.id.menu_accounts_add_account) {
                    startAddAccount()
                }
            }
        }

        return true
    }

    /**
     * Handles actions to be performed when user clicks on a message in list.
     */
    override fun onItemClick(view: View, position: Int) {
        // Get all details about the message from clicked row.
        val current = adapter.getItem(position)

        Intent(this, DisplayMessageActivity::class.java).also { displayMessageIntent ->
            displayMessageIntent.putExtra("sender", current.sender)
            displayMessageIntent.putExtra("subject", current.subject)
            displayMessageIntent.putExtra("date", current.sentDate)
            displayMessageIntent.putExtra("isHtml", current.isHtml)
            displayMessageIntent.putExtra("content", current.content)

            startActivity(displayMessageIntent)
        }
    }

    /**
     * Starts an intent for adding an account, opens AddAccountActivity.
     */
    private fun startAddAccount() {
        val intent = Intent(this, AddAccountActivity::class.java)
        // Expecting result to make it possible for updating the ui.
        startActivityForResult(intent, ADD_ACCOUNT_REQUEST)
    }

    /**
     * Prepare for loading existing accounts into side menu.
     */
    private fun initializeAccountGroup(accounts: List<Account>) {
        // Make sure menu group is empty since all accounts are
        // appended to group on refresh of storage.
        sideMenu.removeGroup(R.id.menu_accounts_group)

        for (account in accounts) {
            sideMenu.add(R.id.menu_accounts_group, Menu.NONE, 0, account.accountName).apply {
                // Make sure settings are applied on each item.
                // Accounts should not be checkable, no need to have account marked as checked when
                // name of selected account is visible in navigation header.
                isCheckable = false
                // Make sure to set if the account should be displayed or hidden.
                // Listing accounts is an opposite action of listing folders.
                isVisible = !listFolders
            }
        }
    }

    /**
     * Initializes the side menu with imap folders for the current selected account.
     */
    private fun initializeFolderGroup(folders: ArrayList<String>) {
        // Remove existing items from group.
        sideMenu.removeGroup(R.id.menu_folders_group)

        for(folder in folders) {
            // Programmatically add imap folder as menu item and make sure it is checkable.
            sideMenu.add(R.id.menu_folders_group, Menu.NONE, 0, folder).apply {
                // Make sure item can be marked as selected.
                isCheckable = true
                // Make sure to set if the folder should be displayed or hidden.
                isVisible = listFolders
            }
        }
    }

    /**
     * Toggle displayAlert state of accounts and folders. Used for toggle groups from onclick actions.
     */
    fun toggleGroupsInSideMenu(view: View) {
        switchMenuState()
    }

    /**
     * Switches what menu groups should be displayed in side menu.
     */
    private fun switchMenuState() {
        // Switch state of menu.
        listFolders = !listFolders

        // Make sure menu is in correct state.
        performToggleGroupsInSideMenu(null)
    }

    /**
     * Gets toggle state after activity reload, the state is based upon if repository has stored a selected account.
     */
    private fun initialToggle() {
        // Get the name of current account.
        val accountName = accountListViewModel.getCurrentAccount()

        if(accountName != null) {
            listFolders = !listFolders
        }

        performToggleGroupsInSideMenu(accountName)
    }

    /**
     * Toggle displayAlert state of accounts and folders.
     */
    private fun performToggleGroupsInSideMenu(accountName: String?) {
        toggleDisplayAccountFolders()
        toggleDisplayAccounts()
        setIcons(accountName)
    }

    /**
     * Hide or show folders for an selected account.
     */
    private fun toggleDisplayAccountFolders() {
        // Hide or show standard folders.
        sideMenu.setGroupVisible(R.id.menu_folders_group, listFolders)
    }

    /**
     * Hide or show added accounts.
     */
    private fun toggleDisplayAccounts() {
        // Negate listFolders variable since displayAlert accounts is an opposite action to displayAlert folders.
        sideMenu.setGroupVisible(R.id.menu_accounts_group, !listFolders)
        sideMenu.setGroupVisible(R.id.menu_accounts_settings_group, !listFolders)
    }

    /**
     * Sets icon and text in sidemenu. Toggles different icons depending on if accounts or folders should be listed.
     */
    private fun setIcons(accountName: String?) {
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        // Get navigation header since the label or icon cannot be found otherwise.
        val header = navigationView.getHeaderView(0)

        if (listFolders) {
            if(accountName != null) {
                // Look for the label inside of navigation header and set to account name.
                header.findViewById<TextView>(R.id.labelSelectedAccount).text = accountName
            }

            // Set icon to tell user it is possible to list accounts.
            header.findViewById<ImageView>(R.id.menuArrow)
                .setImageResource(R.drawable.ic_baseline_arrow_drop_down_24px)
        } else {
            // Set icon to tell user it is possible to displayAlert folders for account.
            header.findViewById<ImageView>(R.id.menuArrow)
                .setImageResource(R.drawable.ic_baseline_arrow_drop_up_24px)
        }
    }

    /**
     * Handle results of activities.
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if(requestCode == ADD_ACCOUNT_REQUEST && resultCode == Activity.RESULT_OK && intent != null) {
            performAddAccount(intent)
        }

        if(requestCode == SEND_MESSAGE_REQUEST && resultCode == Activity.RESULT_OK && intent != null) {
            performSendMessage(intent)
        }
    }

    /**
     * Extracts account details from intent extra and hands it over to viewModel for storage.
     */
    private fun performAddAccount(intent: Intent) {
        val accountName = intent.getStringExtra("accountName")
        val fullName = intent.getStringExtra("fullName")
        val imapHost = intent.getStringExtra("imapHost")
        val imapPort = intent.getIntExtra("imapPort", 0)
        val imapUser = intent.getStringExtra("imapUser")
        val imapPassword = intent.getStringExtra("imapPassword")
        val smtpHost = intent.getStringExtra("smtpHost")
        val smtpPort = intent.getIntExtra("smtpPort", 0)
        val emailAddress = intent.getStringExtra("emailAddress")
        val smtpUser = intent.getStringExtra("smtpUser")
        val smtpPassword = intent.getStringExtra("smtpPassword")

        accountListViewModel.add(
            Account(
                0,
                accountName,
                fullName,
                imapHost,
                imapPort,
                imapUser,
                imapPassword,
                smtpHost,
                smtpPort,
                emailAddress,
                smtpUser,
                smtpPassword
            )
        )
    }

    /**
     * Extract message details from intent and tell message viewmodel to send the message.
     */
    private fun performSendMessage(intent: Intent) {
        val recipients = intent.getStringArrayExtra(Intent.EXTRA_EMAIL)
        val subject = intent.getStringExtra(Intent.EXTRA_SUBJECT)
        val message = intent.getStringExtra(Intent.EXTRA_TEXT)
        val attachments = intent.getParcelableArrayListExtra<Uri>(Intent.EXTRA_STREAM)

        messageListViewModel.sendMessage(recipients, subject, message, attachments)
    }
}
