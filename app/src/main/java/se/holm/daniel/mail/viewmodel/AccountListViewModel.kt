package se.holm.daniel.mail.viewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import se.holm.daniel.mail.MailApplication
import se.holm.daniel.mail.MailRepository
import se.holm.daniel.mail.db.entity.Account
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

/**
 * Handles communication between views related to listing accounts and data access repository.
 */
class AccountListViewModel(application: Application) : AndroidViewModel(application) {
    // Setup of kotlin concurrency helpers, this performs any heavy data access outside
    // of the main thread.
    private var parentJob = Job()
    private val coroutineContext: CoroutineContext get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    // Keeps an up-to-date list with all accounts.
    val accounts: LiveData<List<Account>>
    private val app: MailApplication = application as MailApplication
    private val repository: MailRepository = app.getRepository()

    /**
     * Sets reference to liveData used for displaying list of accounts.
     */
    init {
        accounts = repository.getAccounts()
    }

    /**
     * Returns the name of current active account.
     */
    fun getCurrentAccount(): String? {
        return repository.getCurrentAccountName()
    }

    /**
     * Changes the current active account. Performs request on another thread.
     */
    fun changeCurrentAccount(accountName: String) = scope.launch(Dispatchers.IO) {
        try {
            // Change current active account.
            repository.changeCurrentAccount(accountName)

            // Load folders.
            repository.loadFolders()
        } catch (exception: Exception) {
            repository.setAlert(Pair("Could Not Connect!", "Could not change and connect to the selected account!"))
        }
    }

    /**
     * Add a new account to storage. Performs request on another thread.
     */
    fun add(account: Account) = scope.launch(Dispatchers.IO) {
        try {
            repository.insertAccount(account)
        } catch (exception: Exception) {
            repository.setAlert(Pair("Could Not Insert Into Database!", "The data could not be stored, Please try again!"))
        }
    }
}