package se.holm.daniel.mail.model

/**
 * Class used to hold message data since javamail message objects will throw network on main thread when trying to
 * extract content in recycler view.
 */
data class MessageHolder(
    val subject: String,
    val sender: String,
    val sentDate: String,
    val content: String,
    val isHtml: Boolean
)