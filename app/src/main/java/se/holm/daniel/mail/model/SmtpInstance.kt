package se.holm.daniel.mail.model

import android.content.ContentResolver
import android.net.Uri
import android.provider.OpenableColumns
import se.holm.daniel.mail.db.entity.Account
import java.util.*
import javax.activation.DataHandler
import javax.mail.Message
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.util.ByteArrayDataSource
import kotlin.collections.ArrayList

/**
 * Handles message creation and smtp communication for a specific account.
 */
class SmtpInstance(private val account: Account) {

    private val transport: Transport
    private val session: Session

    /**
     * This block is run on first creation of an instance. Sets information
     * that does not need to change for later usage.
     */
    init {
        // Prepare reusable settings for message transport.
        val properties = System.getProperties()
        properties["mail.smtp.starttls.enable"] = true
        properties["mail.smtp.localhost"] = account.smtpHost

        session = Session.getInstance(properties, null)
        transport = session.getTransport("smtp")
    }

    /**
     * Constructs and sends an email message using smtp.
     */
    fun sendMessage(
        recipients: Array<String>,
        subject: String,
        content: String,
        attachments: ArrayList<Uri>?,
        contentResolver: ContentResolver?
    ) {
        // Prepare addresses for javamail usage.
        val addresses = prepareRecipientAddresses(recipients)

        // Construct message to be sent to addresses
        val message = constructMessage(addresses, subject, content, attachments, contentResolver)

        performSendMessage(message, addresses)
    }

    /**
     * Construct the different mime parts of email message.
     */
    private fun constructMessage(
        address: Array<InternetAddress>,
        subject: String,
        content: String,
        attachments: ArrayList<Uri>?,
        contentResolver: ContentResolver?
    ): Message {
        val message = MimeMessage(session)
        message.setFrom(InternetAddress(account.mailAddress))


        message.setRecipients(Message.RecipientType.TO, address)
        message.subject = subject

        // Message is to consist of several parts, i.e. content and attachments.
        var multipartMime = MimeMultipart()

        // Prepare and add text content to multipart.
        val mimeContentPart = MimeBodyPart()
        mimeContentPart.setText(content)

        // Add content part to message parts.
        multipartMime.addBodyPart(mimeContentPart)

        multipartMime = prepareAttachments(attachments, multipartMime, contentResolver)

        message.setContent(multipartMime)
        message.sentDate = Date()

        return message
    }

    /**
     * Open a connection to smtp server for sending message to provided addresses, closes the connection after
     * successful transport.
     */
    private fun performSendMessage(message: Message, addresses: Array<InternetAddress>) {
        // Only connect when is about to send an message.
        transport.connect(account.smtpHost, account.smtpPort, account.smtpUser, account.smtpPassword)
        transport.sendMessage(message, addresses)
        transport.close()
    }

    /**
     * Create wrapper objects for each string that are representing recipient address. Required by java mail.
     */
    private fun prepareRecipientAddresses(recipients: Array<String>): Array<InternetAddress> {
        // Did not find any way to create array of non nullable type,
        // so use list which can be converted.
        val converted = ArrayList<InternetAddress>()

        // Create special wrapper required by java mail for recipient addresses.
        recipients.forEach {
            converted.add(InternetAddress(it.trim()))
        }

        // Return array of correct type without any nullable objects.
        return converted.toTypedArray()
    }

    /**
     * Make sure java mail can attach files from android system. Opens an input stream of file from uri. The mime type
     * of a uri can easily be extracted, but not the file name. Filename must be fetched from a specific column
     * associated with uri.
     */
    private fun prepareAttachments(
        attachments: ArrayList<Uri>?,
        multipart: MimeMultipart,
        contentResolver: ContentResolver?
    ): MimeMultipart {
        attachments?.forEach { uri ->
            // Open the file from uri for reading.
            contentResolver?.openInputStream(uri)?.use { stream ->
                // Java mail holder for current mail part.
                val currentPart = MimeBodyPart()
                // Easily extract mime type from uri details.
                val mimeType = contentResolver.getType(uri)
                var name = ""

                // Extract file name from column associated with the content uri.
                contentResolver.query(
                    uri,
                    null,
                    null,
                    null,
                    null
                )?.use { cursor ->
                    val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)

                    cursor.moveToFirst()
                    name = cursor.getString(nameIndex)
                }

                // Inject attachment into the mime part.
                val data = ByteArrayDataSource(stream, mimeType)
                currentPart.dataHandler = DataHandler(data)
                currentPart.fileName = name

                // Add the attachment to the multipart message.
                multipart.addBodyPart(currentPart)
            }
        }

        return multipart
    }
}